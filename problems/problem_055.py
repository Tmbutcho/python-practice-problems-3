# Write a function that meets these requirements.
#
# Name:       simple_roman
# Parameters: one parameter that has a value from 1
#             to 10, inclusive
# Returns:    the Roman numeral equivalent of the
#             parameter value
#
# All examples
#     * input: 1
#       returns: "I"
#     * input: 2
#       returns: "II"
#     * input: 3
#       returns: "III"
#     * input: 4
#       returns: "IV"
#     * input: 5
#       returns: "V"
#     * input: 6
#       returns: "VI"
#     * input: 7
#       returns: "VII"
#     * input: 8
#       returns: "VIII"
#     * input: 9
#       returns: "IX"
#     * input: 10
#       returns:  "X"

def simple_roman(variable):
    if variable == 1:
        return "I"
    elif variable == 2:
        return "II"
    elif variable == 3:
        return "III"
    elif variable == 4:
        return "IV"
    elif variable == 5:
        return "V"
    elif variable == 6:
        return "VI"
    elif variable == 7:
        return "VII"
    elif variable == 8:
        return "VIII"
    elif variable == 9:
        return "IX"
    elif variable == 10:
        return "X"


print(simple_roman(10))

#input: variable - int between 1 and 10

#ouput: The roman equivalent of the number

#


# finished!!!
