# Write a function that meets these requirements.
#
# Name:       only_odds
# Parameters: a list of numbers
# Returns:    a copy of the list that only includes the
#             odd numbers from the original list
#
# Examples:
#     * input:   [1, 2, 3, 4]
#       returns: [1, 3]
#     * input:   [2, 4, 6, 8]
#       returns: []
#     * input:   [1, 3, 5, 7]
#       returns: [1, 3, 5, 7]





#input: a list of numbers

#ouput: A copy of list that only has the odd numbers from the original list

#tools: use modulo and the not equal expression. Use the append method.

#pseudocode
# create a new list
# go through the original list.
# take only the odd numbers and add them to the new list.
# return the new list.

# how to determine if a value is odd.
#



def only_odds(list_n):
    new_list = []
    for n in list_n:
        if n % 2 != 0:
            new_list.append(n)
    return new_list


print(only_odds([1, 3,,10, 12, 40, 5, 7]))


# finished!!!!
