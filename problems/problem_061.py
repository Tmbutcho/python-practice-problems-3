# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
#     * input:   [1, 1, 1, 1]
#       returns: [1]
#     * input:   [1, 2, 2, 1]
#       returns: [1, 2]
#     * input:   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]



# input: a list of values

 #ouput: a copy of list but with no duplicates

#tools: append method.

def remove_duplicates(list_v):
    new_list = []
    for v in list_v:
        if v not in new_list:
            new_list.append(v)
    return new_list


print(remove_duplicates([1, 3, 20, 3, 2, 2]))


# finished!!!
