# Write a function that meets these requirements.
#
# Name:       check_input
# Parameters: one parameter that can hold any value
# Returns:    if the value of the parameter is the
#             string "raise", then it should raise
#             a ValueError. otherwise, it should
#             just return the value of the parameter
#
# Examples
#    * input:   3
#      returns: 3
#    * input:   "this is a string"
#      returns: "this is a string"
#    * input:   "raise"
#      RAISES:  ValueError

def check_input(any_value):
    if any_value == "raise":
        raise ValueError
    else:
        return any_value

print(check_input("ice"))


# finished!!!

# input: any value - whatever data type you want

#ouput: The value inputted    or if you inputted "raise", it raises a ValueError.
