# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    largest = 0
    second_largest = 0
    for v in values:
        if v > largest:
            largest = v

    for v in values:
        if v > second_largest and v < largest:
            second_largest = v

    return second_largest


print(find_second_largest([10, 23, 110, 222]))


#finished!!!
