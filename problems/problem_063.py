# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem



# input: a string that has one word      ouput: a new string with all letters


def shift_letters(word):
    shifted_string = ""


    for letter in word:
        digit = ord(letter)
        digit = digit + 1
        n_letter = chr(digit)
        if n_letter == "{":
            n_letter = "a"
        shifted_string = shifted_string + n_letter
    return shifted_string


print(shift_letters("zap"))

# finished!!!!
