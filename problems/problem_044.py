# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

def translate(key_list, dictionary):
    new_list = []
    for key, value in dictionary.items():
        if key in key_list:
            new_list.append(value)
    return new_list



fam = {
    "Mom": "Ndella",
    "Brother": "Noah",
    "Cousin": "Moulaye",
    "Self": "Timothee",
}

print(translate(["Mom", "Brother"], fam))

# finished!!!


# inputs: a list of keys, a dictionary

# ouput: values of the corresponding keys
