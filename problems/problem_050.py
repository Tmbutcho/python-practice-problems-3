# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]


def halve_the_list(list):
    half_of_list = len(list)/2
    list_1 = []
    list_2 = []

    for n in list:
        if len(list_1) < half_of_list:
            list_1.append(n)
        else:
            list_2.append(n)

    return list_1, list_2



print(halve_the_list([1, 2, 3, 4, 5]))


# finished!!!
