# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7


def sum_two_numbers(num_1, num_2):
    return num_1 + num_2



print(sum_two_numbers(10, 20))

# finished!!!!
