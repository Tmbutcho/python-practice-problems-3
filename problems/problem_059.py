# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one


# input: no input
# ouput: a random number between 10 and 500, includive that is divisible by 5 and 7.


# choice() ouputs a random value from the list or string given as an input.

from random import choice


def specific_random():

    random_list = []
    for i in range(10, 501):
        if i % 5 == 0 and i % 7 == 0:
            random_list.append(i)

    return choice(random_list)


print(specific_random())

# finished!!!

# tools - range function, append method, choice function(imported from random)
