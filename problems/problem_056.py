# Write a function that meets these requirements.
#
# Name:       num_concat
# Parameters: two numerical parameters
# Returns:    the concatenated string conversions
#             of the numerical parameters
#
# Examples:
#     input:
#       parameter 1: 3
#       parameter 2: 10
#     returns: "310"
#     input:
#       parameter 1: 9238
#       parameter 2: 0
#     returns: "92380"

def num_concat(num_1, num_2):
    string_number_1 = str(num_1)
    string_number_2 = str(num_2)
    return string_number_1 + string_number_2


print(num_concat(9238, 0))


# finished!!!


#input: num_1, num_2  - both integers

#ouput: the concatenated string conversions of numerical parameters

#tools: str function
