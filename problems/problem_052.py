# Write a function that meets these requirements.
#
# Name:       generate_lottery_numbers
# Parameters: none
# Returns:    a list of six random unique numbers
#             between 1 and 40, inclusive
#
# Example bad results:
#    [4, 2, 3, 3, 1, 5] duplicate numbers
#    [1, 2, 3, 4, 5] not six numbers
#
# You can use randint from random, here, or any of
# the other applicable functions from the random
# package.
#
# https://docs.python.org/3/library/random.html


from random import randint
def generate_lottery_numbers():

    lottery_number_l = []
    for i in range(6):
        lottery_number = randint(1, 40)
        if lottery_number not in lottery_number_l:
            lottery_number_l.append(lottery_number)
    return lottery_number_l

print(generate_lottery_numbers())

# finished!!!!!!!

#input: no input

#output: a list of six random numbers  - list of int

# tools: used the range function, randint function, and append method.
