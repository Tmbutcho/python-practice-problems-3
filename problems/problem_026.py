# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    average = sum(values)/len(values)
    a_condition = average >= 90
    b_condition = average >= 80 and average < 90
    c_condition = average >= 70 and average < 80
    d_condition = average >= 60 and average < 70

    if a_condition:
        return "A", average
    elif b_condition:
        return "B", average
    elif c_condition:
        return "C", average
    elif d_condition:
        return "D", average
    else:
        return "F", average


print(calculate_grade([80, 90, 92, 70]))

#finished!!!
